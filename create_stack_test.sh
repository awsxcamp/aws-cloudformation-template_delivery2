#!/bin/bash
#create a kinesis stream and two lambda functions 

aws cloudformation create-stack \
    --template-body file://test.json \
    --stack-name test \
    --capabilities CAPABILITY_IAM \
    --parameters \
        ParameterKey=S3BucketLambda,ParameterValue='functions-lambda' \
        ParameterKey=S3BucketGuids,ParameterValue='guids' 
        