#!/bin/bash
#create a kinesis stream and two lambda functions 

aws cloudformation update-stack \
    --template-body file://xl-email.json \
    --stack-name xl-email \
    --capabilities CAPABILITY_IAM \
    --parameters \
        ParameterKey=S3BucketLambda,ParameterValue='functions-lambda' \
        ParameterKey=S3BucketGuids,ParameterValue='guids' 
